var chai = require('chai');
var expect = chai.expect;
var fs = require('fs');
var path = require('path');
var Mower = require('../mower.js');
var Monitor = require('../mower-monitor.js');

describe('Monitor', function () {

  var input;

  before(function() {
    path.join(__dirname, 'test');
    input = fs.readFileSync(__dirname+"/"+"input.txt", "utf-8");
  });

  it('Should init two mowers with the right parameters', function() {
    var firstMower = new Mower(5,5,1,2,'N');
    var secondMower = new Mower(5,5,3,3,'E');
    var firstMowerInstructions = "GAGAGAGAA";
    var secondMowerInstructions = "AADAADADDA";
    var monitor = new Monitor(input);

    expect(monitor.getMower(0).mower.position()).to.deep.equal(firstMower.position());
    expect(monitor.getMower(0).mower.direction()).to.deep.equal(firstMower.direction());
    expect(monitor.getMower(0).instructions).to.equal(firstMowerInstructions);

    expect(monitor.getMower(1).mower.position()).to.deep.equal(secondMower.position());
    expect(monitor.getMower(1).mower.direction()).to.deep.equal(secondMower.direction());
    expect(monitor.getMower(1).instructions).to.equal(secondMowerInstructions);

  })

  it('Should sequentially run all mowers', function() {

    var monitor = new Monitor(input);
    var expectedResult = "1 3 N\n4 1 E";

    expect(monitor.start()).to.equal(expectedResult);
  })



})
