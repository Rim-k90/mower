var chai = require('chai');
var expect = chai.expect;
var Mower = require('../mower.js');

describe('mower', function() {

  beforeEach(function() {

  });

  it('Should face North when facing East and turn left', function () {

    var mower = new Mower(5,5,0,0,'E');

    mower.turnLeft();

    expect(mower.direction()).to.equal('N');

  });

  it('Should face East when facing South and turn left', function () {

    var mower = new Mower(5,5,0,0,'S');

    mower.turnLeft();

    expect(mower.direction()).to.equal('E');

  });

  it('Should face West when facing North and turn left', function () {

    var mower = new Mower(5,5,0,0,'N');

    mower.turnLeft();

    expect(mower.direction()).to.equal('W');

  });

  it('Should face South when facing West and turn left', function () {

    var mower = new Mower(5,5,0,0,'W');

    mower.turnLeft();

    expect(mower.direction()).to.equal('S');

  });

  it('Should face North when facing West and turn right', function () {

    var mower = new Mower(5,5,0,0,'W');

    mower.turnRight();

    expect(mower.direction()).to.equal('N');

  });

  it('Should face East when facing North and turn right', function () {

    var mower = new Mower(5,5,0,0,'N');

    mower.turnRight();

    expect(mower.direction()).to.equal('E');

  });

  it('Should face West when facing South and turn right', function () {

    var mower = new Mower(5,5,0,0,'S');

    mower.turnRight();

    expect(mower.direction()).to.equal('W');

  });

  it('Should face South when facing East and turn right', function () {

    var mower = new Mower(5,5,0,0,'E');

    mower.turnRight();

    expect(mower.direction()).to.equal('S');

  });

  it('Should not move when the next position is out of the grid', function () {

    var mower = new Mower(6,5,0,4,'N');
    var expectedPosition = mower.position();

    mower.move();

    expect(mower.position()).to.equal(expectedPosition);

  });

  it('Should head North when facing North ', function () {

    var mower = new Mower(5,5,0,0,'N');
    var expectedPosition = {
      x: 0,
      y: 1
    };

    mower.move();

    expect(mower.position()).to.deep.equal(expectedPosition);

  });

  it('Should head East when facing East', function () {

    var mower = new Mower(5,5,0,0,'E');
    var expectedPosition = {
      x: 1,
      y: 0
    };

    mower.move();

    expect(mower.position()).to.deep.equal(expectedPosition);

  })

  it('Should head West when facing West', function () {
    var mower = new Mower(5,5,2,1,'W');
    var expectedPosition = {
      x: 1,
      y: 1
    };

    mower.move();

    expect(mower.position()).to.deep.equal(expectedPosition);
  })

  it('Should head South when facing South', function () {
    var mower = new Mower(5,5,2,2,'S');
    var expectedPosition = {
      x: 2,
      y: 1
    };

    mower.move();

    expect(mower.position()).to.deep.equal(expectedPosition);

  })

 it('Should end on at position 1 3 N', function () {
   var mower = new Mower(5,5,1,2,'N');
   var expectedEndPosition = {
     x: 1,
     y: 3
   };
   var expectedEndDirection = 'N';

   mower.start("GAGAGAGAA");

   expect(mower.position()).to.deep.equal(expectedEndPosition);
   expect(mower.direction()).to.deep.equal(expectedEndDirection);

 })

  it('Should end at the position 4 1 E', function () {
    var mower = new Mower(5,5,3,3,'E');

    var expectedEndPosition = {
      x: 4,
      y: 1
    };
    var expectedEndDirection = 'E';

    mower.start("AADAADADDA");

    expect(mower.position()).to.deep.equal(expectedEndPosition);
    expect(mower.direction()).to.deep.equal(expectedEndDirection);
  })
})
