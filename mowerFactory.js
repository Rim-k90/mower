var Mower = require('./mower.js');

module.exports = function () {

  this.getMowers = function (size, instructionLines) {
    let mowers = [];

    while(instructionLines.length > 1) {

      let mowerInitialSate = instructionLines.shift();
      let mowerInstructions = instructionLines.shift();
      let mower = createMower(size, mowerInitialSate);

      mowers.push({
                    mower: mower,
                    instructions: mowerInstructions
                  });
    }

    return mowers;
  }

  function createMower(size, mowerState) {
    var state = mowerState.split(' ');
    var x = parseInt(state[0]);
    var y = parseInt(state[1]);
    var direction = state[2];

    return new Mower(size.width, size.height, x, y, direction);
  }

}
