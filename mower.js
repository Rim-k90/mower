var Mower = function (w, h, x, y, dir) {

  var position = {
      x: x,
      y: y
  }

  var direction = dir;
  var gridWidth = w;
  var gridHeight = h;

  this.move = function () {
    switch(direction) {
      case 'N' : moveNorth();
                 break;
      case 'E' : moveEast();
                 break;
      case 'W' : moveWest();
                 break;
      case 'S' : moveSouth();
                 break;
    }

    return position;
  }

  var moveNorth = function () {
    if (isValidPosition(position.x, position.y+1) ) {
      position.y = position.y + 1;
    }

    return position;
  }

  var moveEast = function () {
    if (isValidPosition(position.x+1, position.y) ) {
      position.x = position.x + 1;
    }

    return position;
  }

  var moveWest = function () {
    if (isValidPosition(position.x-1, position.y) ) {
      position.x = position.x - 1;
    }

    return position;
  }

  var moveSouth = function () {
    if (isValidPosition(position.x, position.y-1) ) {
      position.y = position.y - 1;
    }

    return position;
  }

  var isValidPosition = function(x, y) {
    return x >= 0 && x < gridWidth &&
           y >= 0 && y < gridHeight;
  }

  this.turnLeft = function () {
    switch(direction) {
      case 'N' : direction = 'W';
                 break;
      case 'E' : direction = 'N';
                 break;
      case 'W' : direction = 'S';
                 break;
      case 'S' : direction = 'E';
                 break;
    }
  }

  this.turnRight = function () {
    switch(direction) {
      case 'N' : direction = 'E';
                 break;
      case 'E' : direction = 'S';
                 break;
      case 'W' : direction = 'N';
                 break;
      case 'S' : direction = 'W';
                 break;
    }
  }

  this.position = function () {
    return position;
  }

  this.direction = function () {
    return direction;
  }

  this.start = function (instructions) {
    var commands = instructions.split('');
    commands.forEach(execute);

    return {position: position, direction: direction};
  }

  var left = this.turnLeft;
  var right = this.turnRight;
  var head = this.move;

  var execute = function (command) {

    if (command == 'D') {
      right();
    }
    else if (command == 'G') {
      left();
    }
    else {
      head();
    }
  }
}

module.exports = Mower;
