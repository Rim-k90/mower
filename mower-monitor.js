var Mower = require('./mower.js');
var MowerFactory = require('./mowerFactory.js');

function Monitor (input) {

  var mowers;
  var size;
  var mowerFactory = new MowerFactory();

  function init() {
    mowers = parseInput(input);
  }

  init();

  this.start = function() {
    for (let m of mowers) {
      let mower = m.mower;
      mower.start(m.instructions);
    }

    return this.getMowersPositions();
  }

  this.getMower = function(index) {
    return mowers[index];
  }

  this.getSize = function() {
    return size;
  }

  this.getMowersPositions = function() {
    var result = "";
    for (let m of mowers) {
      result += m.mower.position().x;
      result += " " + m.mower.position().y;
      result += " " + m.mower.direction();
      result += "\n";
    }

    return result.trim();
  }

  function parseInput(data) {

    var instructionLines = data.split('\n');
    var sizeInfo = instructionLines.shift();
    size = getSize(sizeInfo);
    return mowerFactory.getMowers(size, instructionLines);

  }

  function getSize(sizeString) {

    var size = sizeString.split(' ');
    return { width: size[0], height: size[1] };

  }

}

module.exports = Monitor;
